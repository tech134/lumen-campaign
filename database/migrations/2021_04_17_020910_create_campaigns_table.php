<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign', function (Blueprint $table) {
            $table->increments('_id');
            $table->string('campaign_id')->unique();
            $table->text('campaign_key')->nullable();
            $table->string('channel_id')->nullable();
            $table->string('campaign_name')->nullable();
            $table->string('industry')->nullable();
            $table->int('contract')->nullable();
            $table->int('duration')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('objective')->nullable();
            $table->int('kpi_leads')->nullable();
            $table->int('kpi_impression')->nullable();
            $table->int('kpi_video')->nullable();
            $table->string('email_client')->nullable();
            $table->string('cc1')->nullable();
            $table->string('cc2')->nullable();
            $table->string('cc3')->nullable();
            $table->string('cc4')->nullable();
            $table->string('attr_1')->nullable();
            $table->string('attr_2')->nullable();
            $table->string('attr_3')->nullable();
            $table->string('attr_4')->nullable();
            $table->string('attr_5')->nullable();
            $table->string('crm')->nullable();
            $table->text('crm_url')->nullable();
            $table->string('crm_method')->nullable();
            $table->text('accessKey')->nullable();
            $table->text('secretKey')->nullable();
            $table->string('chat_platform')->nullable();
            $table->text('chat_access_key')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign');
    }
}