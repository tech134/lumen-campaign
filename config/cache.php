<?php
return [ 
    'cluster' => false,
    'redis' => [
        'driver' => 'redis',
        'connection' => 'default',
    ],
];