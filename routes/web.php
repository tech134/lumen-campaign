<?php
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CampaignController;
use App\Http\Controllers\FilterController;
use App\Http\Controllers\UtmController;
use Illuminate\Support\Facades\Redis;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
 

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth'], function () use ($router) {
   // Matches "/api/register
   $router->post('register', 'AuthController@register');
    $router->post('login', 'AuthController@authenticate');
    $router->post('refresh_token', 'AuthController@refresh_token');
});

$router->group(['middleware' => 'auth'], function () use ($router) {
       $router->post('auth/logout', 'AuthController@logout');
      $router->post('auth/refresh', 'AuthController@refresh');
      $router->post('auth/check', 'AuthController@check');
});

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('api/campaign',  ['uses' => 'CampaignController@index']);
    $router->post('api/campaign',  ['uses' => 'CampaignController@store']);
    $router->get('api/campaign/{campaign_id}',  ['uses' => 'CampaignController@show']);
    $router->patch('api/campaign/{campaign_id}',  ['uses' => 'CampaignController@update']);
    $router->patch('api/campaign/refresh_key/{campaign_id}',  ['uses' => 'CampaignController@refresh_key']);
    $router->patch('api/campaign/delete/{campaign_id}',  ['uses' => 'CampaignController@destroy']);
});

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('api/virtual_number',  ['uses' => 'VirtualNumberController@index']);
    $router->post('api/virtual_number',  ['uses' => 'VirtualNumberController@store']);
    $router->get('api/virtual_number/{campaign_id}',  ['uses' => 'VirtualNumberController@show']);
    $router->patch('api/virtual_number/{campaign_id}',  ['uses' => 'VirtualNumberController@update']);
    $router->patch('api/virtual_number/delete/{campaign_id}',  ['uses' => 'VirtualNumberController@destroy']);
});

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('api/filter_leads',  ['uses' => 'FilterController@index']);
    $router->post('api/filter_leads',  ['uses' => 'FilterController@store']);
    $router->get('api/filter_leads/{id}',  ['uses' => 'FilterController@show']);
    $router->patch('api/filter_leads/{id}',  ['uses' => 'FilterController@update']);
    $router->patch('api/filter_leads/delete/{id}',  ['uses' => 'FilterController@destroy']);
});

$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('api/utm_medium',  ['uses' => 'UtmController@index']);
    $router->post('api/utm_medium',  ['uses' => 'UtmController@store']);
    $router->get('api/utm_medium/{id}',  ['uses' => 'UtmController@show']);
    $router->patch('api/utm_medium/{id}',  ['uses' => 'UtmController@update']);
    $router->patch('api/utm_medium/delete/{id}',  ['uses' => 'UtmController@destroy']);
});


$router->group(['middleware' => 'chat.api'], function () use ($router) {
    $router->post('api/lead-chat',
    [
       'uses' => 'ChatController@store'
    ]
);
});

$router->group(['middleware' => 'form.api'], function () use ($router) {
    $router->post('api/lead-form',
    [
       'uses' => 'FormController@store'
    ]
);
});

$router->group(['middleware' => 'call.api'], function () use ($router) {
    $router->post('api/lead-call',
    [
       'uses' => 'CallController@store'
    ]
    );
});

$router->get('api/test',  ['uses' => 'TestController@index']);
