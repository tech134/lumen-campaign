<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Utm_medium;

use Redirect,Response;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;


class UtmController extends BaseController
{
    public function index(Request $request)
    {
    $data = Utm_medium::orderBy('created_at', 'asc')->get();
    return response()->json($data, 201);
    }

        function destroy(Request $request, $id)
    {
        $data = Utm_medium::where('id',$id)->delete();

        $this->success = "Delete success";

        return response()->json($this->success, 201);

    }

    function store(Request $request)
    {

        $this->validate($request, [
        'utm_medium' => 'required|unique:utm_medium',
        'auto' => 'required',
        ]);

        $this->id = $request->get('id');
        
        $product = new Utm_medium();
        $product->utm_medium = $request->get('utm_medium');
        $product->auto = $request->get('auto');

        $product->save();
        
        $this->success = "Create success";

        return response()->json($this->success, 201);    
    }
    
    function show(Request $request, $id)
    {
    $data = Utm_medium::where('id',$id)->first();

        if (!$data) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Utm medium does not exist.'
            ], 400);
        }

    return response()->json($data, 201);
    }

        function update(Request $request, $id) 
    {

    $data = Utm_medium::where('id',$id)->first();
        if (!$data) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Utm medium does not exist.'
            ], 400);
        }

    $data->utm_medium = $request->get('utm_medium');
    $data->auto = $request->get('auto');
    $data->save();  

    $this->success = "Update success";

    
        return response()->json($this->success, 201);

    }

}

