<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Campaign;
use App\Models\VirtualNumber;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;

use Redirect, Response;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;


class TestController extends BaseController
{

    public function index(Request $request)
    {
        $campaign = Cache::remember("campaign_all", 10 * 60, function () {
            return \DB::table('campaign')->orderby('created_at', 'ASC')->get();
        });

        return response()->json([
            "campaign" => empty($campaign) ? "" : $campaign,
            // "virtual number" => empty($campaign) ? "" : $campaign,
        ], 200);
    }

}
