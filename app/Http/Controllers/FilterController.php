<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Leads_filter;

use Redirect,Response;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;


class FilterController extends BaseController
{
    public function index(Request $request)
    {
    $data = Leads_filter::orderBy('created_at', 'asc')->get();
    return response()->json($data, 201);
    }

        function destroy(Request $request, $id)
    {
        $data = Leads_filter::where('id',$id)->delete();

        $this->success = "Delete success";

        return response()->json($this->success, 201);

    }

    function store(Request $request)
    {

        $this->validate($request, [
        'status' => 'required|unique:leads_filter',
        'filter' => 'required',
        ]);

        $this->id = $request->get('id');
        
        $product = new Leads_filter();
        $product->filter = $request->get('filter');
        $product->status = $request->get('status');

        $product->save();
        
        $this->success = "Create success";

        return response()->json($this->success, 201);    
    }
    
    function show(Request $request, $id)
    {
    $data = Leads_filter::where('id',$id)->first();

        if (!$data) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Filter does not exist.'
            ], 400);
        }

    return response()->json($data, 201);
    }

        function update(Request $request, $id) 
    {

    $data = Leads_filter::where('id',$id)->first();
        if (!$data) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Leads filter does not exist.'
            ], 400);
        }

    $data->filter = $request->get('filter');
    $data->status = $request->get('status');
    $data->save();  

    $this->success = "Update success";

    
        return response()->json($this->success, 201);

    }

}

