<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Campaign;
use App\Models\VirtualNumber;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;

use Redirect,Response;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;


class CampaignController extends BaseController
{

    public function latest($count)
    {
        $key = 'latest.' . $count;
        $cacheKey = $this->getCacheKey($key);

        var_dump($cacheKey);

        return Cache::remember($cacheKey, 60, function() {
            return Campaign::orderBy('campaign_id', 'desc')->take($count)->get();
        });
    }

    public function index(Request $request)
    {

    // $campaign = Campaign::find("TEST001")->virtual_number;
    // $number = VirtualNumber::find(2);

    // $campaign = $number->my_campaign;

    // $campaign = Campaign::orderby('created_at','ASC')->get();
    $campaign = Cache::remember("campaign_all", 10 * 60, function () {
        return Campaign::orderby('created_at','ASC')->get();
    });
    // $number = $campaign->virtual_number;

    // return response()->json($number, 201);

    return response()->json([
            "campaign" => empty($campaign) ? "" : $campaign,
            // "virtual number" => empty($campaign) ? "" : $campaign,
    ], 200);

    return response()->json($campaign, 201);
    }

        function destroy(Request $request, $campaign_id)
    {
        $data = Campaign::where('campaign_id',$campaign_id)->delete();

        $this->success = "success";

        return response()->json($this->success, 201);

    }

    function store(Request $request)
    {

        // $this->validate($request, [
        // 'campaign_id' => 'required|unique:campaign',
        // 'campaign_name' => 'required',
        // 'industry' => 'required',
        // 'contract' => 'required',
        // 'duration' => 'required',
        // 'start_date' => 'required',
        // 'crm' => 'required',
        // 'crm_url' => 'required',
        // 'status' => 'status',
        // ]);

        $this->campaign_id = $request->get('campaign_id');  

        $posts = \DB::table('campaign')->where('campaign_id', \DB::raw("(select max(campaign_id) from campaign)"))->get();
        
        $this->campaign_key = Crypt::encrypt($this->campaign_id);

        $product = new Campaign();
        $product->channel_id = $request->get('channel_id');
        $product->campaign_id = $request->get('campaign_id');
        $product->campaign_name = $request->get('campaign_name');
        $product->campaign_key = $this->campaign_key;
        $product->industry = $request->get('industry');
        $product->contract = $request->get('contract');
        $product->duration = $request->get('duration');
        $product->start_date = $request->get('start_date');
        $product->end_date = $request->get('end_date');
        $product->email_client = $request->get('email_client');
        $product->cc1 = $request->get('cc1');
        $product->cc2 = $request->get('cc2');
        $product->cc3 = $request->get('cc3');
        $product->cc4 = $request->get('cc4');
        $product->attr_1 = $request->get('attr_1');
        $product->attr_2 = $request->get('attr_2');
        $product->attr_3 = $request->get('attr_3');
        $product->attr_4 = $request->get('attr_4');
        $product->attr_5 = $request->get('attr_5');

        $product->crm = $request->get('crm'); 
        $product->crm_url = $request->get('crm_url');
        $product->accessKey = $request->get('accessKey');
        $product->secretKey = $request->get('secretKey');

        $product->chat_platform = $request->get('chat_platform');
        $product->chat_access_key = $request->get('chat_access_key');
        
        $product->objective = $request->get('objective');
        $product->status = $request->get('status');

        $product->ha_apps = $request->get('ha_apps');
        $product->apps_url = $request->get('apps_url');
        $product->save();
        
        $campaign = Campaign::find($this->campaign_id);
          // Set a new key with id
          Redis::set('campaign_' . $this->campaign_id, $campaign);

          return response()->json([
              'status_code' => 201,
              'message' => 'Campaign Inserted',
              'data' => $campaign,
          ]);    
    }
    
    function show(Request $request, $campaign_id)
    {
    // $id = Crypt::decrypt($campaign_id);
    // $data = Campaign::where('campaign_id',$campaign_id)->first();

    // $campaign = Cache::remember("campaign_all", 10 * 60, function () {
    //     return Campaign::find($campaign_id);
    // });

  $campaign_c = Redis::get('campaign_' . $campaign_id);
  if(isset($campaign_c)) {
      $campaign = json_decode($campaign_c, FALSE);
      // return response()->json([
      //     'status_code' => 201,
      //     'message' => 'Fetched from redis',
      //     'data' => $campaign,
      // ]);
  }else {
      $campaign = Campaign::find($campaign_id);
      Redis::set('campaign_' . $campaign_id, $campaign);
      // return response()->json([
      //     'status_code' => 201,
      //     'message' => 'Fetched from database',
      //     'data' => $campaign,
      // ]);
  }
        if (!$campaign) {
        return response()->json([
            'error' => 'Campaign does not exist.'
        ], 400);
        }

      // $number = $campaign->virtual_number;
        // return response()->json($number, 201);

    return response()->json([
            "campaign" => empty($campaign) ? "" : $campaign,
            // "virtual number" => empty($campaign) ? "" : $campaign,
    ], 200);
}


        function update(Request $request, $campaign_id) 
    {

    // $data = Cache::remember("campaign_all", 10 * 60, function () {
    //     return Campaign::where('campaign_id',$campaign_id)->first();
    // });
    // $data = Campaign::where('campaign_id',$campaign_id)->first();
        // if (!$data) {
        //     return response()->json([
        //         'error' => 'Campaign does not exist.'
        //     ], 400);
        // }

    $update = Campaign::findOrFail($campaign_id)->update($request->all());
      if($update) {

          // Delete from Redis
          Redis::del('campaign_' . $campaign_id);

          $campaign = Campaign::find($campaign_id);
          // Set a new key with id
          Redis::set('campaign_' . $campaign_id, $campaign);

          return response()->json([
              'status_code' => 201,
              'message' => 'Campaign updated',
              'data' => $campaign,
          ]);
      }
    }

        function refresh_key(Request $request, $campaign_id) 
    {
    // $time = Carbon::now('Asia/Jakarta')->toDateString(); 

    $campaign_c = Redis::get('campaign_' . $campaign_id);
    if(isset($campaign_c)) {
      $campaign = json_decode($campaign_c, FALSE);
    }else {
      $campaign = Campaign::find($campaign_id);
      Redis::set('campaign_' . $campaign_id, $campaign);
    }

    if (!$campaign) {
        return response()->json([
            'error' => 'Campaign does not exist.'
        ], 400);
    }

    $this->campaign_key = Crypt::encrypt($campaign_id);

    $data = Campaign::find($campaign_id);
    Redis::del('campaign_' . $campaign_id);
    $data->campaign_key =  $this->campaign_key;
    $data->save();  
    
    return response()->json($this->campaign_key, 201);
 
    }
}