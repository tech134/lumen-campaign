<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Campaign;
use App\Models\VirtualNumber;
use Illuminate\Support\Facades\Crypt;

use Redirect,Response;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;


class VirtualNumberController extends BaseController
{
    public function index(Request $request)
    {
    $number = VirtualNumber::all();

    // $campaign = $number->my_campaign;

    // $campaign = VirtualNumber::orderby('created_at','ASC')->get();
    // $number = $campaign->virtual_number;

        // return response()->json($number, 201);

    return response()->json([
            "number" => empty($number) ? "" : $number,
            // "virtual number" => empty($campaign) ? "" : $campaign,
    ], 200);
    }

        function destroy(Request $request, $campaign_id)
    {
        $data = VirtualNumber::where('campaign_id',$campaign_id)->delete();

        $this->success = "success";

        return response()->json($this->success, 201);

    }

    function store(Request $request)
    {

        $this->validate($request, [
        'campaign_id' => 'required',
        'number' => 'required|unique:virtual_number',
        'area' => 'required',
        ]);

        $this->campaign_id = $request->get('campaign_id');

        $data = Campaign::where('campaign_id', $this->campaign_id);

        if (!$data) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Virtual number does not exist.'
            ], 400);
        }

        $product = new VirtualNumber();
        $product->number = $request->get('number');
        $product->campaign_id = $this->campaign_id;
        $product->area = $request->get('area');
        $product->save();
        
        $this->success = "success";

        return response()->json($this->success, 201);    
    }
    
    function show(Request $request, $campaign_id)
    {
    // $id = Crypt::decrypt($campaign_id);
    // $data = Campaign::where('campaign_id',$campaign_id)->first();
    $data = VirtualNumber::where('campaign_id', $campaign_id)->first();

            if (!$data) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Virtual number does not exist.'
            ], 400);
        }

    $campaign = $data->my_campaign;
    //     // return response()->json($number, 201);

    return response()->json([
            "virtual number" => empty($data) ? "" : $data,
    ], 200);

    // return response()->json($campaign, 201);
    }

        function update(Request $request, $campaign_id) 
    {

    $data = VirtualNumber::where('campaign_id',$campaign_id)->first();
        if (!$data) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Virtual Number does not exist.'
            ], 400);
        }

    $data->campaign_id = $request->get('campaign_id');
    $data->number = $request->get('number');
    $data->area = $request->get('area');
    $data->save();  

    $this->success = "success";

    
    return response()->json($this->success, 201);

    }
}

