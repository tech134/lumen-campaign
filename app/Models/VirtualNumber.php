<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VirtualNumber extends Model
{
	protected $table = 'virtual_number';
	// protected $fillable = ['id','campaign_id','number','area'];
    // protected $primaryKey = 'id'; 
//public $incrementing = false;
	protected $guarded = ['id'];


     public function my_campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }
}