<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
	protected $table = 'campaign';
    protected $fillable = ['campaign_id','campaign_name','contract','duration','start_date','end_date',
    'email_client','cc1','cc2','cc3','cc4','attr_1','attr_2','attr_3','attr_4','attr_5','objective','industry','status',
    'crm','crm_url','crm_method','accessKey','secretKey','channel_id','chat_platform','chat_access_key','campaign_key','ha_apps','apps_url'];
    protected $primaryKey = 'campaign_id';
    public $incrementing = false;
    // protected $guarded = ['id'];


        public function virtual_number()
    {
        return $this->hasMany(VirtualNumber::class,'Campaign_id');
    }
}

